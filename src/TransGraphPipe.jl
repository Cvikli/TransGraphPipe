module TransGraphPipe

using GraphPipe: graph_ref
using GraphPipe.GraphWorld: Graph

using Random
using EllipsisNotation
using Distributions
using Distributed
using Flux: softmax, normalise
using Boilerplate: @sizes, @typeof

let singleton_transformations_data= nothing
	global function init_transformations(mode=:NOTHINGG, graph_base = "_2_graphsets/")
		if singleton_transformations_data === nothing
			singleton_transformations_data = init_transformations_w1(mode, graph_base)
		end
		singleton_transformations_data
	end
end


function init_transformations_w1(mode, graph_base)
	known_transformations = Graph[]
	for tt in sort(readdir(graph_base * "transformations"))
		path = graph_base * "transformations/" * tt
		if isfile(path)
			graph = graph_ref("transformations/" * tt, what="Transformation", allow_warnings=true, prefix=graph_base)
			# getproperty.(graph.nodes, :symbol) fails at materialization command so not precompileable... LOL SHIT :D
			if mode !== :RNN && any(n.symbol in [:state, :zstate] for n in graph.nodes)
				continue
			end
			push!(known_transformations, graph)
		end
	end
	ACTION_SPACE = length(known_transformations)
	transformation_noop_id = 1	
	return known_transformations, ACTION_SPACE, transformation_noop_id
end


# init_transformations()
precompile(init_transformations,())
precompile(init_transformations,(Symbol,)) 
precompile(init_transformations,(Symbol, String))
end # Transformations
##%